﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArchAPI.Model
{
    public class GenericResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultType Result { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }

        public GenericResponse(ResultType result, dynamic data, string message = "")
        {
            Result = result;
            Message = message;
            Data = data;
        }
    }


    public enum ResultType
    {
        success,
        failed
    }
}
