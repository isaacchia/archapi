﻿using ArchAPI.Model;
using ArchData.Model;
using ArchData.Service;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace ArchAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly ILogService _logService;

        public OrderController(IOrderService orderService, ILogService logService, IAuthService authService): base(authService)
        {
            _orderService = orderService;
            _logService = logService;
        }


        [HttpGet]
        public async Task<ActionResult> Get()
        {

            try
            {
                Token token = GetToken(await HttpContext.GetTokenAsync("access_token"));
                var result = await _orderService.getOrders();
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return Ok(new GenericResponse(ResultType.success, null));
                }
            }
            catch (Exception ex)
            {
                _logService.LogError(ex.ToString(), "");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.ToString());
            }

        }


        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> GetById(int id, [FromQuery(Name = "custom")] int isCustom)
        {

            try
            {
                Token token = GetToken(await HttpContext.GetTokenAsync("access_token"));
                if (isCustom == 1)
                {
                    var result = await _orderService.getOrderByIdWithCustomField(id);
                    if (result != null)
                    {
                        // return GenericResponse("", result);
                        return Ok(new GenericResponse(ResultType.success, result));
                    }
                    else
                    {
                        return Ok(new GenericResponse(ResultType.success, null));
                    }

                }
                else
                {
                    var result = await _orderService.getOrderById(id);
                    if (result != null)
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return Ok(new GenericResponse(ResultType.success, null));
                    }

                }
            }
            catch (Exception ex)
            {
                _logService.LogError(ex.ToString(), "");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.ToString());
            }




        }


    }
}
