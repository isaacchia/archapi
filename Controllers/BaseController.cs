﻿using ArchData.Model;
using ArchData.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArchAPI.Controllers
{
   
    public class BaseController : ControllerBase
    {
        IAuthService _authService;

        protected BaseController(IAuthService authService)
        {
            _authService = authService;
        }

        public Token GetToken(string encodedString)
        {
           return _authService.ParseToken(encodedString);
        } 
    }
}
